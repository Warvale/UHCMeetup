package net.warvale.uhcmeetup.listeners;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.managers.GameState;
import net.warvale.uhcmeetup.player.MeetupPlayer;
import net.warvale.uhcmeetup.player.PlayerManager;
import net.warvale.uhcmeetup.utils.BungeeUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Random;

public class SpecListener implements Listener {

    private final Inventory alive = Bukkit.createInventory(null, 54, ChatColor.GREEN + "Players Alive:");

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        ItemStack itemStack = player.getItemInHand();

        if (itemStack != null && PlayerManager.getInstance().getMeeupPlayer(player.getUniqueId()).isVanishedPlayer()) {
            if (itemStack.getType().equals(Material.DIAMOND)) {
                player.openInventory(this.getAlive());
            } else if (itemStack.getType().equals(Material.SKULL_ITEM)) {
                this.randomPlayer(player);
            } else if (itemStack.getType().equals(Material.ENCHANTED_BOOK)) {
                //todo: Find a meetup server that is in the lobby state
            } else if (itemStack.getType().equals(Material.WATCH)) {
                BungeeUtils.sendPlayer(player, "Hub-1");
            }
        }

    }

    @EventHandler
    public void onInventoryInteract(InventoryClickEvent e) {
        Inventory inv = e.getInventory();
        ItemStack item = e.getCurrentItem();
        Player player = (Player) e.getWhoClicked();

        if (inv.getName().equalsIgnoreCase(ChatColor.GREEN + "Players Alive")) {
            SkullMeta meta = (SkullMeta) item.getItemMeta();
            Player target = Bukkit.getPlayerExact(meta.getOwner());

            if (target != null) {
                player.teleport(target.getLocation());
                player.sendMessage(ChatColor.GREEN + "You have teleported to " + target.getName());
            }

            player.closeInventory();
        }
    }

    private Inventory getAlive() {
        this.alive.clear();
        for (MeetupPlayer meetupPlayer : PlayerManager.getInstance().getAlivePlayers()) {
            Player player;
            if (!meetupPlayer.isAliveAndPlaying() || (player = Bukkit.getServer().getPlayer(meetupPlayer.getName())) == null) continue;

            ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            meta.setOwner(player.getName());
            meta.setDisplayName("\u00a7a" + player.getName());
            skull.setItemMeta(meta);

            //this.alive.addItem(UHCMeetup.getGame().newItem(Material.SKULL_ITEM, "\u00a7a" + player.getName(), 3));
            this.alive.addItem(skull);
        }
        return this.alive;
    }

    private void randomPlayer(Player player) {
        if (UHCMeetup.getGame().isState(GameState.INGAME)) {
            ArrayList<String> arrayList = new ArrayList<>();

            for (MeetupPlayer meetupPlayer : PlayerManager.getInstance().getAlivePlayers()) {
                if (!meetupPlayer.isAliveAndPlaying()) continue;
                arrayList.add(meetupPlayer.getName());
            }

            if (arrayList.size() == 0) {
                return;
            }

            Random random = new Random();
            int n = random.nextInt(arrayList.size());

            Player target = Bukkit.getServer().getPlayer(arrayList.get(n));
            if (target != null) {
                player.teleport(target.getLocation());
                player.sendMessage(ChatColor.GOLD + "You have randomly teleported to " + target.getName());
            } else {
                player.sendMessage(ChatColor.RED + "Could not find a random player to teleport to!");
            }
        } else {
            player.sendMessage(ChatColor.RED + "The game hasn't started yet!");
        }
    }

}
