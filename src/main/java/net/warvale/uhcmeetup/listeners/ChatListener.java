package net.warvale.uhcmeetup.listeners;

import net.warvale.uhcmeetup.player.MeetupPlayer;
import net.warvale.uhcmeetup.player.PlayerManager;
import net.warvale.uhcmeetup.utils.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import net.warvale.uhcmeetup.teams.TeamManager;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        if (TeamManager.getInstance().isTeamsEnabled()) {
            e.setFormat(TeamManager.getInstance().getTeam(e.getPlayer()).getPrefix() + ChatColor.RESET + " " + e.getPlayer().getName() + " " +
                    ChatColor.DARK_GRAY + "» " + ChatColor.RESET + e.getMessage());
        } else {

            if (PlayerManager.getInstance().getMeeupPlayer(e.getPlayer().getUniqueId()).isVanishedPlayer()) {
                sendSpecMessage(ChatColor.AQUA + "[Spectator]" + ChatUtils.getPrefix(e.getPlayer()) +
                        ChatColor.RESET + " " + e.getPlayer().getName() + " " + ChatColor.DARK_GRAY + "» " + ChatColor.RESET + e.getMessage());
                e.setCancelled(true);
                return;
            }

            e.setFormat(ChatUtils.getPrefix(e.getPlayer()) +
                    ChatColor.RESET + " " + e.getPlayer().getName() + " " + ChatColor.DARK_GRAY + "» " + ChatColor.RESET + e.getMessage());
        }

    }

    private void sendSpecMessage(String string) {
        for (MeetupPlayer meetupPlayer : PlayerManager.getInstance().getSpectators()) {
            Player player;
            if (!meetupPlayer.isVanishedPlayer() || (player = Bukkit.getServer().getPlayer(meetupPlayer.getName())) == null) continue;
            player.sendMessage(string);
        }
    }

}
