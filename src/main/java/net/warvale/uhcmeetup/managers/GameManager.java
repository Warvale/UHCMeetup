package net.warvale.uhcmeetup.managers;

import net.warvale.uhcmeetup.UHCMeetup;
import org.bukkit.*;
import org.bukkit.entity.Player;
import net.warvale.uhcmeetup.config.ConfigManager;
import net.warvale.uhcmeetup.tasks.BorderRunnable;
import net.warvale.uhcmeetup.teams.TeamManager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class GameManager {

    private int maxPlayers;
    private int minPlayers;

    private boolean pvp;
    private String worldName;
    private int currentBorder;
    private int shrinkTime;
    private GameState state;
    private GameType type;
    private Set<Player> players = new HashSet<>();
    private Set<Player> spectators = new HashSet<>();
    private ArrayList<String> teamChatColors = new ArrayList<>();
    private ArrayList<String> usedTeamChatColors = new ArrayList<>();
    private BukkitRunnable borderTask = null;

    public GameManager() {
        //game settings
        this.minPlayers = ConfigManager.getConfig().getInt("settings.minPlayers", 8);
        this.maxPlayers = ConfigManager.getConfig().getInt("settings.maxPlayers", 100);
        this.worldName = ConfigManager.getConfig().getString("settings.world", "uhcworld");

        this.pvp = false;
        this.state = GameState.LOADING;

        if (ConfigManager.getConfig().getBoolean("teams.enabled", false)) {
            this.type = GameType.TEAMS;
            TeamManager.getInstance().setTeamsEnabled(true);
            TeamManager.getInstance().setMaxSize(ConfigManager.getConfig().getInt("teams.size", 2));
        } else {
            this.type = GameType.FFA;
        }

        this.currentBorder = ConfigManager.getConfig().getInt("settings.borderSize", 125);
        this.shrinkTime = ConfigManager.getConfig().getInt("settings.shrinkTime", 3);

        for (ChatColor ch : ChatColor.values()) { // Load chat colors into colors list
            if (ch != ChatColor.BLACK && ch != ChatColor.BOLD && ch != ChatColor.ITALIC && ch != ChatColor.MAGIC
                    && ch != ChatColor.RESET && ch != ChatColor.STRIKETHROUGH && ch != ChatColor.UNDERLINE) {
                this.teamChatColors.add(ch.toString());
            }
        }
    }

    public void setPVP(boolean pvp) {
        this.pvp = pvp;
    }

    public boolean pvpEnabled() {
        return this.pvp;
    }

    public String getWorldName() {
        return this.worldName;
    }

    public World getWorld() {
        return Bukkit.getWorld(this.worldName);
    }

    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
        ConfigManager.getConfig().set("minPlayers", minPlayers);
        ConfigManager.getInstance().saveConfig();
    }

    public int getMinPlayers() {
        return this.minPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        ConfigManager.getConfig().set("maxPlayers", maxPlayers);
        ConfigManager.getInstance().saveConfig();
    }

    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    public void setCurrentBorder(int radius) {
        this.currentBorder = radius;
    }

    public int getCurrentBorder() {
        return this.currentBorder;
    }

    public GameState getState() {
        return this.state;
    }

    public boolean isState(GameState state) {
        return this.getState().equals(state);
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public GameType getType() {
        return this.type;
    }

    public boolean isType(GameType type) {
        return getType().equals(type);
    }

    public void setType(GameType type) {
        this.type = type;
    }

    public void setShrinkTime(int shrinkTime) {
        this.shrinkTime = shrinkTime;
    }

    public int getShrinkTime() {
        return this.shrinkTime;
    }

    public void startBorderShrink() {
        Bukkit.broadcastMessage(UHCMeetup.PREFIX + ChatColor.GOLD + "The border will now shrink every " + this.shrinkTime + " minutes until 10x10!");

        BorderRunnable borderRunnable = new BorderRunnable();
        borderRunnable.runTaskTimerAsynchronously(UHCMeetup.getInstance(), this.shrinkTime * 20, this.shrinkTime * 20);
        setBorderTask(borderRunnable);
    }

    private static int teamColorNumber = 0;
    public static ChatColor[] validTeamColors = new ChatColor[] {
            ChatColor.DARK_GREEN, /*ChatColor.DARK_AQUA, ChatColor.DARK_RED,*/ ChatColor.DARK_PURPLE, ChatColor.GOLD,
            ChatColor.GRAY, ChatColor.BLUE, ChatColor.GREEN, /*ChatColor.AQUA,*/ ChatColor.RED, ChatColor.LIGHT_PURPLE,
            ChatColor.YELLOW, ChatColor.WHITE
    };

    public ChatColor getRandomTeamChatColor() {
        return GameManager.validTeamColors[teamColorNumber++ % GameManager.validTeamColors.length];
    }

    public Location getScatterLocation() {
        Random random = new Random();
        int radius1 = random.nextInt(this.currentBorder * 2) - this.currentBorder;
        int radius2 = random.nextInt(this.currentBorder * 2) - this.currentBorder;
        return new Location(this.getWorld(), (double) radius1, (double)this.getWorld().getHighestBlockYAt(radius1, radius2) + 0.5, (double)radius2);
    }

    public Set<Player> getSpectators() {
        return this.spectators;
    }

    public BukkitRunnable getBorderTask() {
        return borderTask;
    }

    public void setBorderTask(BukkitRunnable borderTask) {
        this.borderTask = borderTask;
    }

    public ItemStack newItem(Material material, String string, int n) {
        ItemStack itemStack = new ItemStack(material, 1, (short) n);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(string);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

}
