package net.warvale.uhcmeetup.hook.hooks;

import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.caching.MetaData;
import me.lucko.luckperms.api.caching.UserData;
import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.hook.Hook;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.ServicesManager;

import java.util.logging.Level;

public class LuckPermsHook extends Hook{

    private LuckPermsApi permsApi = null;

    public LuckPermsHook() {
        super("LuckPermsHook");
    }

    @Override
    public void initialize() {

        try {

            Plugin lpTest = Bukkit.getServer().getPluginManager().getPlugin("LuckPerms");
            if (lpTest == null || !lpTest.isEnabled())
            {
                UHCMeetup.getInstance().getLogger().log(Level.WARNING, "LuckPerms is not present, so the integration was disabled.");
                this.setEnabled(false);
                return;
            }

            ServicesManager manager = Bukkit.getServicesManager();

            if (manager.isProvidedFor(LuckPermsApi.class)) {
                permsApi = manager.getRegistration(LuckPermsApi.class).getProvider();
                this.setEnabled(true);
            }

        } catch (Exception ex) {
            UHCMeetup.getInstance().getLogger().log(Level.WARNING, "LuckPerms could not be hooked into, so the integration was disabled.");
            this.setEnabled(false);
        }

    }

    @Override
    public String getPrefix(Player player) {
        User user = permsApi.getUser(player.getUniqueId());
        UserData cachedData = null;
        Contexts contexts = null;
        MetaData metaData = null;

        if (user != null) {
            cachedData = user.getCachedData();
            contexts = permsApi.getContextsForPlayer(player);
            metaData = cachedData.getMetaData(contexts);
        }

        if (metaData != null) {
            return metaData.getPrefix() != null ? ChatColor.translateAlternateColorCodes('&', metaData.getPrefix()) : "";
        }

        return "";
    }

}
