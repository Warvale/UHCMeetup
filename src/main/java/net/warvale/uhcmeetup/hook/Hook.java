package net.warvale.uhcmeetup.hook;

import org.bukkit.entity.Player;

public abstract class Hook {

    private String name;
    private boolean enabled = false;

    protected Hook(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void initialize();

    public boolean isEnabled() {
        return enabled;
    }

    protected void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public abstract String getPrefix(Player player);
}
