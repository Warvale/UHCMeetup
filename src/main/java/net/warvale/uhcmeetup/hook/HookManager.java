package net.warvale.uhcmeetup.hook;

import net.warvale.uhcmeetup.hook.hooks.LuckPermsHook;

import java.util.ArrayList;
import java.util.List;

public class HookManager {

    private final List<Hook> hooks = new ArrayList<>();

    public HookManager() {
        this.registerHooks();
    }

    private void registerHooks() {
        hooks.add(new LuckPermsHook());

        for (Hook hook : hooks) {
            hook.initialize();
        }
    }

    public Hook getHook(String name) {
        for (Hook hook : hooks) {
            if (hook.getName().equals(name)) {
                return hook;
            }
        }

        return null;
    }

    public Hook getHook(Object hookClass) {
        for (Hook hook : hooks) {
            if (hook.getClass().equals(hookClass)) {
                return hook;
            }
        }

        return null;
    }

}
