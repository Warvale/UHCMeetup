package net.warvale.uhcmeetup.tasks;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.player.MeetupPlayer;
import net.warvale.uhcmeetup.player.PlayerManager;
import net.warvale.uhcmeetup.scoreboards.GameScoreboard;
import net.warvale.uhcmeetup.teams.MeetupTeam;
import net.warvale.uhcmeetup.teams.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class GameTask extends BukkitRunnable {

    private int seconds = 0;
    private int shrinkTime = UHCMeetup.getGame().getShrinkTime() * 60;
    private String ffaWin = ChatColor.GOLD + "Congratulations to %player% for winning the UHC Meetup!";

    public void run() {
        seconds++;

        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            GameScoreboard.getInstance().updateScoreboard(player);
        }

        if (this.shrinkTime == this.seconds) {
            UHCMeetup.getGame().startBorderShrink();
        }

        if (TeamManager.getInstance().isTeamsEnabled()) {

            if (TeamManager.getInstance().getTeamsAlive() == 1) {
                MeetupTeam meetupTeam = TeamManager.getInstance().getLastTeam();

                StringBuilder sb = new StringBuilder(ChatColor.GOLD + "Congratulations to ");

                for (UUID uuid : meetupTeam.getPlayers()) {
                    Player player = Bukkit.getPlayer(uuid);
                    if (player != null) {
                        sb.append(player.getName()).append(" and ");
                    }
                }

                sb.append(" for winning the UHC Meetup!");

                Bukkit.broadcastMessage(UHCMeetup.PREFIX + sb.toString());

                if (UHCMeetup.getGame().getBorderTask() != null) {
                    UHCMeetup.getGame().getBorderTask().cancel();
                }

                this.cancel();
                new RestartTask().runTaskTimer(UHCMeetup.getInstance(), 20, 20);
            }

        } else {
            if (PlayerManager.getInstance().getAlivePlayers().size() == 1) {

                //todo: cancel tasks if any are running?
                for (MeetupPlayer player : PlayerManager.getInstance().getAlivePlayers()) {
                    Bukkit.broadcastMessage(UHCMeetup.PREFIX + ffaWin.replace("%player%", player.getName()));
                }

                if (UHCMeetup.getGame().getBorderTask() != null) {
                    UHCMeetup.getGame().getBorderTask().cancel();
                }

                this.cancel();
                new RestartTask().runTaskTimer(UHCMeetup.getInstance(), 20, 20);
            }
        }

    }


}
