package net.warvale.uhcmeetup.tasks;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.utils.BungeeUtils;
import net.warvale.uhcmeetup.utils.SoundUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class RestartTask extends BukkitRunnable {

    private int countdown = 11;

    @Override
    public void run() {
        countdown--;

        if (countdown > 0) {

            if (countdown <= 10) {
                this.broadcastShutdown(countdown);
            }

        } else {
            this.cancel();

            if (UHCMeetup.getInstance().isBungeeEnabled()) {
                for (Player online : Bukkit.getServer().getOnlinePlayers()) {
                    BungeeUtils.sendPlayer(online, UHCMeetup.getInstance().getLobbyName());
                }
            }

            //delayed shutdown
            Bukkit.getServer().getScheduler().runTaskLater(UHCMeetup.getInstance(), new Runnable() {
                @Override
                public void run() {
                    UHCMeetup.getInstance().getServer().shutdown();
                }
            }, 5);
        }

    }

    private void broadcastShutdown(int seconds) {
        Bukkit.broadcastMessage(UHCMeetup.PREFIX + ChatColor.GOLD + "Server restarting in " + seconds + " seconds!");
        SoundUtils.playSound(Sound.NOTE_STICKS);
    }

}
