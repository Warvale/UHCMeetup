package net.warvale.uhcmeetup.tasks;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.managers.GameState;
import net.warvale.uhcmeetup.utils.BungeeUtils;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class BungeeAlertTask extends BukkitRunnable {

    @Override
    public void run() {

        if (UHCMeetup.getGame().isState(GameState.LOBBY)
                && Bukkit.getServer().getOnlinePlayers().size() < UHCMeetup.getGame().getMinPlayers()) {
            BungeeUtils.networkBroadcast(UHCMeetup.PREFIX + "&6A game needs more players. &bClick here &6to join.", "AlertGame");
        }

    }

}
