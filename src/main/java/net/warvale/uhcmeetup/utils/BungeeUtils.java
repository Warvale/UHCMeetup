package net.warvale.uhcmeetup.utils;

import com.google.common.collect.ImmutableList;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.warvale.uhcmeetup.UHCMeetup;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class BungeeUtils {

    public static void networkBroadcast(String globalMessage, String type) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        try {
            out.writeUTF("UHCMeetup");

            // MORE FUCKIN STREAMS
            ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
            DataOutputStream msgout = new DataOutputStream(msgbytes);

            //the broadcast type
            msgout.writeUTF(type);

            //the message
            msgout.writeUTF(globalMessage);

            //the server name
            msgout.writeUTF(UHCMeetup.getServerName());

            out.writeShort(msgbytes.toByteArray().length);
            out.write(msgbytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        if (UHCMeetup.getInstance().getServer().getOnlinePlayers().size() > 0) {
            ImmutableList.copyOf(UHCMeetup.getInstance().getServer().getOnlinePlayers()).get(0).sendPluginMessage(UHCMeetup.getInstance(), "BungeeCord", b.toByteArray());
        }
    }

    public static void sendPlayer(Player player, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        player.sendPluginMessage(UHCMeetup.getInstance(),"BungeeCord", out.toByteArray());
    }


}
