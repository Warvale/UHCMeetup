package net.warvale.uhcmeetup.utils;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.hook.hooks.LuckPermsHook;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatUtils {

    public static String getPrefix(Player player) {

        if (UHCMeetup.getHookManager().getHook(LuckPermsHook.class).isEnabled()) {
            return UHCMeetup.getHookManager().getHook(LuckPermsHook.class).getPrefix(player);
        }

        return "";

    }

}
