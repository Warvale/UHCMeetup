package net.warvale.uhcmeetup.utils;

import com.mrpowergamerbr.temmiewebhook.DiscordEmbed;
import com.mrpowergamerbr.temmiewebhook.DiscordMessage;
import com.mrpowergamerbr.temmiewebhook.TemmieWebhook;
import com.mrpowergamerbr.temmiewebhook.embed.ThumbnailEmbed;
import net.warvale.api.servers.ServerType;
import net.warvale.uhcmeetup.UHCMeetup;

import java.util.Arrays;

public class DiscordUtils {

    public static String statusHookURl;

    public static final int GENERAL_MSG = 3447003;
    public static final int SUCCESS_MSG = 0x23c540;
    public static final int ERROR_MSG = 0xfd0006;
    public static final int WARNING_MSG = 0xfff30d;


    public static void sendFailedBoot() {
        TemmieWebhook temmie = new TemmieWebhook(statusHookURl);

        ThumbnailEmbed te = new ThumbnailEmbed();
        te.setUrl("https://i.imgur.com/sLw1okg.png?width=83&height=73");
        te.setWidth(83);
        te.setHeight(73);

        DiscordEmbed de = DiscordEmbed.builder()
                .title("* UHCMeetup Server Failed Booting!*")
                .description("A UHCMeetup server failed to boot, please investigate!")
                .color(ERROR_MSG)
                .thumbnail(te)
                .build();

        DiscordMessage dm = DiscordMessage.builder()
                .username("UHCMeetup Status") // We are creating a message with the username "DevotedStatus Bot"...
                .content("") // with no content because we are going to use the embed...
                .embeds(Arrays.asList(de)) // with the our embed...
                .build(); // and now we build the message!

        temmie.sendMessage(dm);
    }

    public static void sendStatusUpdate(String message, int color) {
        TemmieWebhook temmie = new TemmieWebhook(statusHookURl);

        DiscordEmbed de = DiscordEmbed.builder()
                .title("UHCMeetup Status") // We are creating a embed with this title...
                .description(message)
                .color(color)
                .build();

        DiscordMessage dm = DiscordMessage.builder()
                .username(UHCMeetup.getServerName()) // We are creating a message with the username "DevotedStatus Bot"...
                .content("") // with no content because we are going to use the embed...
                .embeds(Arrays.asList(de)) // with the our embed...
                .build(); // and now we build the message!

        temmie.sendMessage(dm);
    }

}
