package net.warvale.uhcmeetup.bungee;

import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.warvale.uhcmeetup.bungee.listeners.ChannelListener;
import net.warvale.uhcmeetup.bungee.redis.RedisManager;

public class UHCMeetupBungee extends Plugin {

    private static UHCMeetupBungee instance;

    @Getter private static boolean redisEnabled = false;

    @Override
    public void onEnable() {
        UHCMeetupBungee.instance = this;

        Plugin redisBungee = ProxyServer.getInstance().getPluginManager().getPlugin("RedisBungee");
        if (redisBungee != null) {
            //enable redis support
            redisEnabled = true;
            RedisManager.get().subscribe();
        }

        //enable the channel listener
        getLogger().info("Registering Channel Listener...");
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ChannelListener());

    }

    public static UHCMeetupBungee getInstance() {
        return instance;
    }

}
