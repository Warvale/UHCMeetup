package net.warvale.uhcmeetup.bungee.redis;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import com.imaginarycode.minecraft.redisbungee.RedisBungeeAPI;
import com.imaginarycode.minecraft.redisbungee.events.PubSubMessageEvent;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.warvale.uhcmeetup.bungee.UHCMeetupBungee;

import java.util.UUID;

public class RedisManager implements Listener {

    private static RedisManager instnace;
    private static RedisBungeeAPI api = RedisBungee.getApi();

    public final static String UHCMEETUP_CHANNEL = "uhcmeetup";

    public static RedisManager get() {
        if (instnace == null) {
            instnace = new RedisManager();
        }
        return instnace;
    }

    public void subscribe(){
        UHCMeetupBungee.getInstance().getProxy().getPluginManager().registerListener(UHCMeetupBungee.getInstance(), this);
        api.registerPubSubChannels(UHCMEETUP_CHANNEL);
    }

    public void publish(String channel, String message){
        api.sendChannelMessage(channel, message);
    }

    @EventHandler
    public void onPubSubMessage(PubSubMessageEvent e) {
        if (e.getChannel().equalsIgnoreCase(UHCMEETUP_CHANNEL)) {
            JsonParser parser = new JsonParser();
            JsonObject gameAlert = parser.parse(e.getMessage()).getAsJsonObject();
            TextComponent clickMsg = new TextComponent(ChatColor.translateAlternateColorCodes('&', gameAlert.get("message").getAsString()));
            clickMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + gameAlert.get("server").getAsString()));

            ProxyServer.getInstance().broadcast(clickMsg);
        }
    }

    public static RedisBungeeAPI getApi() {
        return RedisManager.api;
    }

}
