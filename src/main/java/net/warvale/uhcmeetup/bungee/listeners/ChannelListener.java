package net.warvale.uhcmeetup.bungee.listeners;

import com.google.gson.JsonObject;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.warvale.uhcmeetup.bungee.UHCMeetupBungee;
import net.warvale.uhcmeetup.bungee.redis.RedisManager;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class ChannelListener implements Listener {

    @EventHandler
    public void onPluginMessage(PluginMessageEvent e){

        if (e.getTag().equalsIgnoreCase("BungeeCord")) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(e.getData()));
            try {
                String channel = in.readUTF(); // channel we delivered
                short len = in.readShort();
                byte[] msgbytes = new byte[len];
                in.readFully(msgbytes);

                if (channel.equals("UHCMeetup")) {
                    DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(msgbytes));
                    String msg = msgin.readUTF();
                    String globalMessage = msgin.readUTF();
                    String server = msgin.readUTF();

                    if (msg.startsWith("AlertGame")) {
                        if (UHCMeetupBungee.isRedisEnabled()) {
                            JsonObject gameAlert = new JsonObject();
                            gameAlert.addProperty("message", globalMessage);
                            gameAlert.addProperty("server", server);

                            RedisManager.get().publish(RedisManager.UHCMEETUP_CHANNEL, gameAlert.toString());
                        } else {
                            TextComponent clickMsg = new TextComponent(ChatColor.translateAlternateColorCodes('&', globalMessage));
                            clickMsg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + server));

                            ProxyServer.getInstance().broadcast(clickMsg);
                        }
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
