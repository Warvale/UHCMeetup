package net.warvale.uhcmeetup.commands;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.managers.GameState;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import net.warvale.uhcmeetup.utils.ItemStackUtils;

import java.util.HashSet;
import java.util.List;

public class VanishCommand implements CommandExecutor {

    public static HashSet<Player> players = new HashSet<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            // Don't let regular users use this cmd
            if (!player.isOp()) {
                return true;
            }

            if (args.length > 0) {
                Player p = UHCMeetup.getInstance().getServer().getPlayer(args[0]);
                if (p == null) {
                    player.sendMessage(ChatColor.RED + "Player is not online or doesn't exist.");
                } else {
                    player = p;
                }
            }

            // Vanish whoever now
            if (VanishCommand.players.contains(player)) {
                VanishCommand.unvanishPlayer(player);
            } else {
                VanishCommand.vanishPlayer(player);
            }
        }

        return true;
    }

    public static void vanishPlayer(final Player player) {
        // Hide players
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.hidePlayer(player);
        }

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getInventory().setHelmet(new ItemStack(Material.AIR));
        player.getInventory().setChestplate(new ItemStack(Material.AIR));
        player.getInventory().setLeggings(new ItemStack(Material.AIR));
        player.getInventory().setBoots(new ItemStack(Material.AIR));
        player.spigot().setCollidesWithEntities(false);
        player.setGameMode(GameMode.CREATIVE);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0));
        player.getInventory().setItem(0, ItemStackUtils.createItem(Material.DIAMOND, 1, ChatColor.GREEN + "Players Alive"));
        player.getInventory().setItem(1, ItemStackUtils.createItem(Material.SKULL_ITEM, 1, ChatColor.GREEN + "Random Player"));
        //player.getInventory().setItem(7, ItemStackUtils.createItem(Material.ENCHANTED_BOOK, 1, ChatColor.GREEN + "Play Again"));
        player.getInventory().setItem(8, ItemStackUtils.createItem(Material.WATCH, 1, ChatColor.GREEN + "Return to Hub"));

        if (UHCMeetup.getGame().isState(GameState.LOBBY)) {
            player.teleport(new Location(UHCMeetup.getGame().getWorld(), 0, 100, 0));
        }

        player.sendMessage(ChatColor.GREEN + "You have vanished!");
    }

    public static void unvanishPlayer(Player player) {
        VanishCommand.players.remove(player);

        // Show players
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.showPlayer(player);
        }

        player.setHealth(20);
        player.setFoodLevel(20);
        player.getActivePotionEffects().clear();
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setGameMode(GameMode.SURVIVAL);
        player.setAllowFlight(false);
        player.setFlying(false);
        player.spigot().setCollidesWithEntities(true);
        player.teleport(UHCMeetup.getInstance().getSpawn());

        player.sendMessage(ChatColor.GREEN + "You are now visible!");
    }

}
