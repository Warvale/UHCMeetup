package net.warvale.uhcmeetup.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import net.warvale.uhcmeetup.teams.MeetupTeam;
import net.warvale.uhcmeetup.teams.TeamManager;

public class DebugTeamsCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!sender.hasPermission("uhcmeetup.debug")) {
            sender.sendMessage(ChatColor.RED + "You don't have permission.");
            return false;
        }

        StringBuilder teamList = new StringBuilder("The following teams are currently registered: \n");

        for (MeetupTeam meetupTeam : TeamManager.getInstance().getTeams().values()) {
            teamList.append(meetupTeam.toString()).append("\n");
        }

        sender.sendMessage(teamList.toString());

        return true;
    }

}
