package net.warvale.uhcmeetup.commands;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.managers.GameState;
import net.warvale.uhcmeetup.utils.BungeeUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AlertGameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String name, String[] args) {
        if (command.getName().equalsIgnoreCase("alertgame")) {

            if (!sender.hasPermission("uhcmeetup.alertgame")) {
                sender.sendMessage(ChatColor.RED + "You don't have permission.");
                return true;
            }

            if (UHCMeetup.getGame().isState(GameState.LOBBY) &&
                    Bukkit.getServer().getOnlinePlayers().size() < UHCMeetup.getGame().getMinPlayers()) {
                BungeeUtils.networkBroadcast(UHCMeetup.PREFIX + "&6A game needs more players. &bClick here &6to join.", "AlertGame");
            } else {
                sender.sendMessage(ChatColor.RED + "You can not alert a game while a game is full or is in progress.");
            }

            return true;
        }
        return false;
    }

}
