package net.warvale.uhcmeetup.commands;

import net.warvale.uhcmeetup.UHCMeetup;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.warvale.uhcmeetup.player.MeetupPlayer;
import net.warvale.uhcmeetup.player.PlayerManager;
import net.warvale.uhcmeetup.teams.MeetupTeam;
import net.warvale.uhcmeetup.teams.TeamManager;
import net.warvale.uhcmeetup.teams.request.Request;
import net.warvale.uhcmeetup.teams.request.RequestManager;

import java.util.UUID;

public class TeamCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Only players can use this command!");
            return true;
        }

        if (!TeamManager.getInstance().isTeamsEnabled()) {
            sender.sendMessage(ChatColor.RED + "Teams are currently disabled!");
            return true;
        }

        if (args.length < 1) {
            sendHelp(sender);
            return true;
        }

        Player player = (Player) sender;
        if (args[0].equalsIgnoreCase("list")) {
            if (args.length == 2) {
                player = Bukkit.getPlayer(args[1]);
            }

            if (player != null) {
                MeetupTeam team = TeamManager.getInstance().getTeam(player);
                if (team != null) {
                    this.teamList(sender, team);
                    return true;
                }
                if (player == sender) {
                    sender.sendMessage(ChatColor.RED + "You are not in a team!");
                    sender.sendMessage(UHCMeetup.PREFIX + ChatColor.GOLD + "Use " + ChatColor.GREEN + "/team create "
                            + ChatColor.GOLD + "to create a team");
                    return true;
                }
                sender.sendMessage(ChatColor.RED + player.getName() + " is not in a team!");
                return true;
            }
            sender.sendMessage(ChatColor.RED + "Couldn't find player.");
            return true;
        }

        if (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("solo")) {
            if (TeamManager.getInstance().getTeam(player) != null) {
                sender.sendMessage(ChatColor.RED + "You are already in a team, use /team leave to leave your current team.");
                return true;
            }
            TeamManager.getInstance().createTeam(player);
            sender.sendMessage(ChatColor.GREEN + "Team created! Use /team invite <player>§7 to invite a player.");
            return true;
        }

        if (args[0].equalsIgnoreCase("accept")) {
            Request request = RequestManager.getInstance().getRequest(player);
            if (request == null) {
                sender.sendMessage(ChatColor.RED + "You don't have any pending team invites!");
                return true;
            }
            TeamManager.getInstance().registerTeam(player, request.getTeam());
            RequestManager.getInstance().requestMap.remove(player);
            return true;
        }

        if (args[0].equalsIgnoreCase("deny")) {
            Request request = RequestManager.getInstance().getRequest(player);
            if (request == null) {
                player.sendMessage(ChatColor.RED + "You don't have any pending invites.");
                return true;
            }
            RequestManager.getInstance().declined(player);
            return true;
        }

        if (args[0].equalsIgnoreCase("invite")) {
            if (args.length == 1) {
                player.sendMessage(ChatColor.RED + "/team invite <player>");
                return true;
            }

            Player player2 = Bukkit.getServer().getPlayer(args[1]);
            if (player2 == null) {
                player.sendMessage(ChatColor.RED + "Could not find player!");
                return true;
            }

            if (player2 == player) {
                player.sendMessage(ChatColor.RED + "You cannot invite yourself to the team!");
                return true;
            }

            MeetupTeam team = TeamManager.getInstance().getTeam(player);
            if (team == null) {
                TeamManager.getInstance().createTeam(player);
                team = TeamManager.getInstance().getTeam(player);
                RequestManager.getInstance().sendRequest(player, player2, team);
                return true;
            }

            if (team.getOwner() != player) {
                sender.sendMessage(ChatColor.RED + "You must be the team leader to invite players to the team!");
                return true;
            }

            RequestManager.getInstance().sendRequest(player, player2, team);
            return true;
        }

        if (args[0].equalsIgnoreCase("kick")) {
            if (args.length == 1) {
                player.sendMessage(ChatColor.RED + "/team kick <player>");
                return true;
            }

            MeetupTeam team = TeamManager.getInstance().getTeam(player);
            if (team == null) {
                player.sendMessage(ChatColor.RED + "You are not in a team!");
                return true;
            }

            if (team.getOwner() != player) {
                sender.sendMessage(ChatColor.RED + "You must be the team leader to kick players out of the team!");
                return true;
            }

            OfflinePlayer offlinePlayer = Bukkit.getServer().getOfflinePlayer(args[1]);
            if (offlinePlayer == null) {
                sender.sendMessage(ChatColor.RED + "Could not find player!");
                return true;
            }

            if (offlinePlayer == player) {
                sender.sendMessage(ChatColor.RED + "You cannot kick yourself out of the team, use /team leave to leave the team!");
                return true;
            }

            if (!team.getPlayers().contains(offlinePlayer.getUniqueId())) {
                sender.sendMessage(ChatColor.RED + "This player is not part of your team!");
                return true;
            }

            TeamManager.getInstance().unregisterTeam(offlinePlayer.getUniqueId());
            if (offlinePlayer.isOnline()) {
                offlinePlayer.getPlayer().sendMessage(ChatColor.RED + "You were kicked from the team by " + sender.getName());
            }

            team.sendMessage(ChatColor.RED + "" + offlinePlayer.getName() + " has been kicked from the team.");
            return true;
        }

        if (args[0].equalsIgnoreCase("leave")) {
            MeetupTeam team = TeamManager.getInstance().getTeam(player);
            if (team == null) {
                sender.sendMessage(ChatColor.RED + "You are not part of any team!");
                return true;
            }
            TeamManager.getInstance().unregisterTeam(player.getUniqueId());
            sender.sendMessage(ChatColor.GREEN + "You have left your team.");
            return true;
        }

        return false;
    }

    private void sendHelp(CommandSender sender) {
        sender.sendMessage(ChatColor.GOLD + "-------------------------------");
        sender.sendMessage( ChatColor.DARK_AQUA + "" + ChatColor.UNDERLINE + "Team commands: ");
        sender.sendMessage(" ");
        sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GRAY + "/team create" + ChatColor.RED + " to create a team.");
        sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GRAY + "/team solo" + ChatColor.RED + " to be alone in your team.");
        sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GRAY + "/team invite <player>" + ChatColor.RED + " to invite players to your team.");
        sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GRAY + "/team accept" + ChatColor.RED + " to accept a team invitation.");
        sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GRAY + "/team deny" + ChatColor.RED + " to deny a team invitation.");
        sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GRAY + "/team leave" + ChatColor.RED + " to leave your current team.");
        sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GRAY + "/team kick <player>" + ChatColor.RED + " to kick a player from your team.");
        sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GRAY + "/team list <player>" + ChatColor.RED + " to list someone's team.");
        sender.sendMessage(ChatColor.GOLD + "-------------------------------");
    }

    private void teamList(CommandSender sender, MeetupTeam team) {
        StringBuilder sb = new StringBuilder();
        sb.append(team.getPrefix());
        sb.append(ChatColor.YELLOW);
        sb.append(" Players: ");

        for (UUID uuid : team.getPlayers()) {
            Player pl = UHCMeetup.getInstance().getServer().getPlayer(uuid);
            if (pl != null) {
                MeetupPlayer meetupPlayer = PlayerManager.getInstance().getMeeupPlayer(pl.getUniqueId());

                // Green for alive, red for dead
                if (meetupPlayer.getState() == MeetupPlayer.State.PLAYER || meetupPlayer.getState() == MeetupPlayer.State.SPEC) {
                    sb.append(ChatColor.GREEN);
                    sb.append(pl.getName());
                    sb.append(ChatColor.GOLD);
                    //sb.append(HealthCommand.getHeartsLeftString(ChatColor.GOLD, pl.getHealth()));
                } else {
                    sb.append(ChatColor.RED);
                    sb.append(pl.getName());
                }
            } else {
                MeetupPlayer meetupPlayer = PlayerManager.getInstance().getMeeupPlayer(uuid);

                // Green for alive, red for dead
                if (meetupPlayer.getState() == MeetupPlayer.State.PLAYER || meetupPlayer.getState() == MeetupPlayer.State.SPEC) {
                    sb.append(ChatColor.GREEN);
                } else {
                    sb.append(ChatColor.RED);
                }
            }

            sb.append(ChatColor.YELLOW);
            sb.append(", ");
        }

        sender.sendMessage(sb.substring(0, sb.length() - 2));
    }

}
