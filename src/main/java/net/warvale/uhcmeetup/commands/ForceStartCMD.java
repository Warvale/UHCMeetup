package net.warvale.uhcmeetup.commands;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.managers.GameState;
import net.warvale.uhcmeetup.managers.ScatterManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import net.warvale.uhcmeetup.teams.TeamManager;

public class ForceStartCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("uhcmeetup.forcestart")) {
            sender.sendMessage(ChatColor.RED + "You don't have permission.");
            return false;
        }

        if (!UHCMeetup.getGame().isState(GameState.LOBBY)) {
            sender.sendMessage(ChatColor.RED + "You can only force start the game during the " + GameState.LOBBY.toString() + " state");
            return true;
        }

        //make sure there is more than 1 player on the server
        if (Bukkit.getServer().getOnlinePlayers().size() <= 1) {
            sender.sendMessage(ChatColor.RED + "You can only force start the game with 2 or more players.");
            return true;
        }

        if (TeamManager.getInstance().isTeamsEnabled()) {
            TeamManager.getInstance().autoPlace();
        }

        ScatterManager.getInstance().startScatter(sender);
        return true;
    }

}
