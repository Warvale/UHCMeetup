package net.warvale.uhcmeetup.player;

import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.commands.VanishCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.*;

public class PlayerManager {

    private static PlayerManager instance;
    private Map<UUID, MeetupPlayer> players = new HashMap<>();

    public static PlayerManager getInstance() {
        if (instance == null) {
            instance = new PlayerManager();
        }
        return instance;
    }

    public Map<UUID, MeetupPlayer> getMeetupPlayers() {
        return this.players;
    }

    public MeetupPlayer getMeeupPlayer(UUID playerUUID) {
        return this.players.get(playerUUID);
    }

    public boolean doesMeetupPlayerExsists(UUID playerUUID) {
        return this.players.containsKey(playerUUID);
    }

    public void createMeetupPlayer(UUID playerUUID, MeetupPlayer.State state) {
        this.players.put(playerUUID, new MeetupPlayer(playerUUID, state));
    }

    public void removeMeetupPlayer(UUID playerUUID) {
        if (this.players.containsKey(playerUUID)) {
            this.players.remove(playerUUID);
        }
    }

    public void setSpectating(boolean enable, final Player player) {
        final boolean specEnable = enable;
        final MeetupPlayer meetupPlayer = this.getMeeupPlayer(player.getUniqueId());

        Bukkit.getScheduler().runTaskLater(UHCMeetup.getInstance(), new Runnable() {
            @Override
            public void run() {
                meetupPlayer.setSpec(specEnable);
                if (specEnable) {
                    meetupPlayer.setPlayerAlive(false);
                    VanishCommand.unvanishPlayer(player);
                }
                if (!specEnable && player.isOnline()) {
                    if (UHCMeetup.getGame().getSpectators().contains(player)) {
                        UHCMeetup.getGame().getSpectators().remove(player);
                    }
                    VanishCommand.unvanishPlayer(player);
                }
            }
        }, 25);
    }

    public Set<MeetupPlayer> getSpectators() {
        Set<MeetupPlayer> spectators = new HashSet<>();
        for (MeetupPlayer player : getMeetupPlayers().values()) {
            if (player.isVanishedPlayer()) {
                spectators.add(player);
            }
        }
        return spectators;
    }

    public Set<MeetupPlayer> getAlivePlayers() {
        Set<MeetupPlayer> alive = new HashSet<>();
        for (MeetupPlayer player : getMeetupPlayers().values()) {
            if (player.isAliveAndPlaying()) {
                alive.add(player);
            }
        }
        return alive;
    }

    public int alivePlayers() {
        int n = 0;
        for (MeetupPlayer meetupPlayer : this.players.values()) {
            if (!meetupPlayer.isAliveAndPlaying()) continue;
            ++n;
        }
        return n;
    }

    public int online() {
        int n = 0;
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            ++n;
        }
        return n;
    }

    public int deadPlayers() {
        int n = 0;
        for (MeetupPlayer meetupPlayer : this.players.values()) {
            if (!meetupPlayer.didPlayerDie()) continue;
            ++n;
        }
        return n;
    }

    public int spectators() {
        int n = 0;
        for (MeetupPlayer meetupPlayer : this.players.values()) {
            if (!meetupPlayer.isSpectating()) continue;
            ++n;
        }
        return n;
    }

    public Set<MeetupPlayer> meetupPlayersSet(Set<UUID> set) {
        HashSet<MeetupPlayer> hashSet = new HashSet<>();
        for (UUID playerUUID : set) {
            hashSet.add(this.getMeeupPlayer(playerUUID));
        }
        return hashSet;
    }

}
