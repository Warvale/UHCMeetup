package net.warvale.uhcmeetup.teams;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.*;

public class TeamManager {

    private static TeamManager instance;

    public static final Map<UUID, MeetupTeam> teams = new HashMap<>();
    private boolean teamsEnabled = false;
    private int maxSize = 2;

    public static TeamManager getInstance() {
        if (instance == null) {
            instance = new TeamManager();
        }
        return instance;
    }

    public boolean isTeamsEnabled() {
        return this.teamsEnabled;
    }

    public void setTeamsEnabled(boolean enabled) {
        this.teamsEnabled = enabled;
    }

    public Map<UUID, MeetupTeam> getTeams() {
        return teams;
    }

    public void autoPlace() {
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            MeetupTeam team = teams.get(player.getUniqueId());
            if (team != null) {
                continue;
            }
            this.createTeam(player);
        }
    }

    public void createTeam(Player player) {
        MeetupTeam team = teams.get(player.getUniqueId());
        if (team != null) {
            team.removePlayer(player.getUniqueId());
        }
        this.registerTeam(player, new MeetupTeam(player));
    }

    public void registerTeam(Player player, MeetupTeam team) {
        if (team.getSize() == this.getMaxSize()) {
            player.sendMessage("\u00a7cTeam size cannot be bigger than" + this.maxSize);
            return;
        }
        teams.put(player.getUniqueId(), team);
    }

    public void addTeam(MeetupTeam team) {
        teams.put(team.getOwner().getUniqueId(), team);
    }

    public void disbandTeam(MeetupTeam team) {
        for (UUID uuid : team.getPlayers()) {
            this.unregisterTeam(uuid);
        }
    }

    public void clearTeams() {
        teams.clear();
        for (UUID teamUUID : teams.keySet()) {
            this.unregisterTeam(teamUUID);
        }
    }

    public void unregisterTeam(UUID teamUUID) {
        teams.get(teamUUID).removePlayer(teamUUID);
        teams.remove(teamUUID);
    }

    public int getMaxSize() {
        return this.maxSize;
    }

    public void setMaxSize(int n) {
        this.maxSize = n;
    }

    public Set<MeetupTeam> getTeamSet() {
        HashSet<MeetupTeam> hashSet = new HashSet<>();
        hashSet.addAll(teams.values());
        return hashSet;
    }

    public int getTeamsAlive() {
        int n = 0;
        for (MeetupTeam team : this.getTeamSet()) {
            if (!team.isAlive()) continue;
            ++n;
        }
        return n;
    }

    public MeetupTeam getLastTeam() {
        if (this.getTeamsAlive() == 1) {
            for (MeetupTeam team : this.getTeamSet()) {
                if (!team.isAlive()) continue;
                return team;
            }
        }
        return null;
    }

    public MeetupTeam getTeam(OfflinePlayer offlinePlayer) {
        return getTeams().get(offlinePlayer.getUniqueId());
    }


}
