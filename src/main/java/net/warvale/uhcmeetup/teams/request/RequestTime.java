package net.warvale.uhcmeetup.teams.request;

import net.warvale.uhcmeetup.UHCMeetup;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import net.warvale.uhcmeetup.teams.MeetupTeam;

public class RequestTime {

    public static void requestTimer(final Player player, final MeetupTeam team) {
        new BukkitRunnable(){

            public void run() {
                Request request = RequestManager.getInstance().getRequest(player);
                if (request != null) {
                    RequestManager.getInstance().timedOut(player);
                    player.sendMessage(ChatColor.RED + "Your request from " + team.getOwner().getName() + " timed out!");
                    team.getOwner().sendMessage(ChatColor.RED + "Team invite to " + player.getName() + " timed out!");
                }
            }
        }.runTaskLater(UHCMeetup.getInstance(), 350);
    }

}
