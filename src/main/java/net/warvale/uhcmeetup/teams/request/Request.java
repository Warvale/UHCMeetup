package net.warvale.uhcmeetup.teams.request;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import net.warvale.uhcmeetup.teams.MeetupTeam;

public class Request {

    private MeetupTeam team;
    private Player recipient;

    public Request(MeetupTeam team, Player player) {
        this.team = team;
        this.recipient = player;
    }

    void decline() {
        this.recipient.sendMessage(ChatColor.GREEN + "You have denied the team invite!");
        Player owner = this.team.getOwner();
        owner.sendMessage(ChatColor.RED +  this.recipient.getName() + " denied the team invite!");
    }

    public MeetupTeam getTeam() {
        return this.team;
    }

}
