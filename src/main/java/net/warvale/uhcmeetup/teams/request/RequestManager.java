package net.warvale.uhcmeetup.teams.request;

import net.warvale.uhcmeetup.UHCMeetup;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import net.warvale.uhcmeetup.teams.MeetupTeam;
import net.warvale.uhcmeetup.teams.TeamManager;

import java.util.HashMap;
import java.util.Map;

public class RequestManager implements Listener {

    private static RequestManager instance;
    public Map<Player, Request> requestMap = new HashMap<>();

    public static RequestManager getInstance() {
        if (instance == null) {
            instance = new RequestManager();
        }
        return instance;
    }

    public void register(UHCMeetup plugin) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }


    public void sendRequest(Player player, Player player2, MeetupTeam team) {
        ChatColor chatColor = ChatColor.GOLD;
        ChatColor chatColor2 = ChatColor.DARK_AQUA;
        if (!this.requestMap.containsKey(player2)) {
            if (TeamManager.getInstance().getTeam(player2) == null) {
                this.requestMap.put(player2, new Request(team, player2));
                player2.sendMessage(chatColor + "You have received a team request from: \u00a7r" + team.toString());
                player2.sendMessage(chatColor + "Use \u00a7a/team accept " + chatColor + "or \u00a7c/team deny" + chatColor + " to respond.");
                team.sendMessage("\u00a7a" + team.getOwner().getName() + chatColor + " invited " + chatColor2 + player2.getName() + chatColor + " to the team!");
                RequestTime.requestTimer(player2, team);
            } else {
                player.sendMessage("\u00a7cPlayer is already in a team!");
            }
        } else {
            player.sendMessage("\u00a7cPlayer already has a team request!");
        }
    }

    public Request getRequest(Player player) {
        return this.requestMap.get(player);
    }

    public void declined(Player player) {
        this.requestMap.remove(player).decline();
    }

    public void timedOut(Player player) {
        this.requestMap.remove(player);
    }

}
