package net.warvale.uhcmeetup.teams;

import com.google.common.base.Joiner;
import net.warvale.uhcmeetup.UHCMeetup;
import net.warvale.uhcmeetup.player.MeetupPlayer;
import net.warvale.uhcmeetup.player.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

public class MeetupTeam {

    private static int globalTeamNumber = 0;

    private final Set<UUID> players = new HashSet<>();
    private Player owner;

    private String prefix;
    private int teamNumber;
    private ChatColor chatColor;

    private int kills = 0;
    private Location scatterLocation;

    /**
     * Creates a team with the given team leader
     *
     * @param player - Team leader
     */
    public MeetupTeam(Player player) {
        this.owner = player;
        this.players.add(player.getUniqueId()); //todo: keep this here?

        // lol no
        if (MeetupTeam.globalTeamNumber == 69) {
            MeetupTeam.globalTeamNumber++;
        }

        if (player.getUniqueId().equals(UUID.fromString("e3020512-5ba9-4104-82dc-f1c4dcba553c"))) {
            this.teamNumber = 0;

            // No one else get's to be team 0 ^.^
            if (MeetupTeam.globalTeamNumber == 0) {
                MeetupTeam.globalTeamNumber++;
            }
        } else {
            this.teamNumber = MeetupTeam.globalTeamNumber++;
        }

        this.chatColor = UHCMeetup.getGame().getRandomTeamChatColor();
        this.prefix = this.chatColor + "[Team " + this.teamNumber + "]";
        this.scatterLocation = UHCMeetup.getGame().getScatterLocation();
    }

    public int getSize() {
        return this.getPlayers().size();
    }

    public Player getOwner() {
        return this.owner;
    }

    public Set<UUID> getPlayers() {
        return this.players;
    }

    public void addPlayer(UUID uuid) {
        this.players.add(uuid);
    }

    public void removePlayer(UUID uuid) {
        this.players.remove(uuid);
    }

    public String getPrefix() {
        return prefix;
    }

    public int getTeamNumber() {
        return this.teamNumber;
    }

    public int getKills() {
        return this.kills;
    }

    public void addKill() {
        ++this.kills;
    }

    public boolean isAlive() {
        for (MeetupPlayer meetupPlayer : PlayerManager.getInstance().meetupPlayersSet(this.players)) {
            if (meetupPlayer == null || !meetupPlayer.isPlayerAlive()) continue;
            return true;
        }
        return false;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public void setChatColor(ChatColor chatColor) {
        this.chatColor = chatColor;
    }

    @Override
    public String toString() {
        List<String> names = new ArrayList<>();
        for (UUID uuid : this.players) {
            Player player = Bukkit.getPlayer(uuid);
            if (player == null) continue;
            names.add(player.getName());
        }

        return "Team #" + this.teamNumber + " " + Joiner.on(", ").join(names);
    }

    public void sendMessage(String string) {
        for (UUID uuid : this.getPlayers()) {
            Player player = Bukkit.getPlayer(uuid);
            if (player == null) continue;
            player.sendMessage(this.getPrefix() + " " + string);
        }
    }

    public Location getScatterLocation() {
        return scatterLocation;
    }
}
